package modele;
import java.util.GregorianCalendar;
import java.io.Serializable;
import java.util.Calendar;
 
public class Date implements Comparable <Date>, Serializable {
  private int chJour;
  private int chMois;
  private int chAnnee;
  private GregorianCalendar chDateCalendrier;
  private int chJourSemaine ;  
   
  public Date ()   { 
	  chDateCalendrier = new GregorianCalendar ();
	  chAnnee = chDateCalendrier.get (Calendar.YEAR);
	  chMois = chDateCalendrier.get (Calendar.MONTH)+1; // janvier = 0, fevrier = 1...
	  chJour = chDateCalendrier.get (Calendar.DAY_OF_MONTH);
	  chJourSemaine = chDateCalendrier.get (Calendar.DAY_OF_WEEK);
  }
  
  public Date (int parJour, int parMois, int parAnnee)   {   
	chJour = parJour;
	chMois = parMois;
	chAnnee = parAnnee; 
	chDateCalendrier = new GregorianCalendar (chAnnee,chMois-1,chJour);
	chJourSemaine = chDateCalendrier.get(Calendar.DAY_OF_WEEK);				
  } 
   
  /**
   * retourne 0 si this et parDate sont egales, 
   * -1 si this precede parDate,
   *  1 si parDate precede this
   */
  public int compareTo(Date parDate) {
		if (chAnnee < parDate.chAnnee) 
			return -1;
		if (chAnnee > parDate.chAnnee) 
			return 1;
		//Les ann�es sont �gales, on passe aux mois
		if (chAnnee == parDate.chAnnee) {
			if (chMois > parDate.chMois)
				return -1;
			if (chMois < parDate.chMois)
				return 1;
			//Les mois sont �gaux, on passe aux jours
			if (chMois == parDate.chMois) {
				if (chJour < parDate.chJour)
					return -1;
				if (chJour > parDate.chJour)
					return 1;
				return 0; // Tout est �gal
			}
		}
		return -1;
	}
 
  public Date dateDuLendemain ()   {	
    if (chJour < dernierJourDuMois(chMois,chAnnee))
		     return  new Date (chJour+1,chMois,chAnnee);
		else if (chMois < 12)
				return new Date (1,chMois+1,chAnnee);
			 else return new Date (1,1,chAnnee+1);	
  }  
  
  public Date dateDeLaVeille () {    
	if (chJour > 1)
			return  new Date (chJour-1,chMois,chAnnee);
	else if (chMois > 1)
			   return new Date (Date.dernierJourDuMois(chMois-1, chAnnee),chMois-1,chAnnee);
		 else return  new Date (31,12,chAnnee-1);
  }	 
  
  public static int dernierJourDuMois (int parMois, int parAnnee) {
		switch (parMois) {
			 case 2 : if (estBissextile (parAnnee))  return 29 ; else return 28 ;  
			 case 4 : 	 case 6 : 	 case 9 : 	 case 11 : return 30 ;
			 default : return 31 ;
			}  // switch
	  } 
	  
  private static boolean estBissextile(int parAnnee) {
			return parAnnee % 4 == 0 && (parAnnee % 100 != 0 || parAnnee % 400 == 0);
	  }
    
  public String toString () {
    String chaine = new String();
    switch (chJourSemaine) {
		 case 1: chaine = "dimanche"; break;
		 case 2: chaine = "lundi"; break;
		 case 3: chaine = "mardi"; break;
		 case 4: chaine = "mercredi"; break;
		 case 5: chaine = "jeudi"; break;
		 case 6: chaine = "vendredi"; break;
		 case 7: chaine = "samedi"; break;
		}
	chaine += " " + chJour + " ";
	switch (chMois) {
		 case 1: chaine += "janvier"; break;
		 case 2: chaine += "fevrier"; break;
		 case 3: chaine += "mars"; break;
		 case 4: chaine += "avril"; break;
		 case 5: chaine += "mai"; break;
		 case 6: chaine += "juin"; break;
		 case 7: chaine += "juillet"; break;
		 case 8: chaine += "aout"; break;
		 case 9: chaine += "septembre"; break;
		 case 10: chaine += "octobre"; break;
		 case 11: chaine += "novembre"; break;
		 case 12: chaine += "decembre"; break;
		}	
	return chaine;
  }  


public int getAnnee() { 
	return chAnnee;
}

public int getJour() { 
	return chJour;
}

public int getMois() { 
	return chMois;
}

public int getJourSemaine () {
	return chJourSemaine;
}

/**
 * retourne la date du premier jour de la semaine contenant this 
 * @return
 */
public Date datePremierJourSemaine () {
	Date datePrem = this;	 
	while (datePrem.getJourSemaine()!=2) {
		datePrem = datePrem.dateDeLaVeille();
	}
	return datePrem;
}

public static Date lireDate() throws ExceptionDate { //Constructeur fait les blocs try/catch, pas besoin ici
	System.out.println("Entrez la date sous forme dd mm aaaa");
	String chaine = Clavier.lireString(); //lecture de la date
	String[] tabFrag = chaine.split("\\s"); //fragmente � chaque espace, stocke les fragments dans un tableau tabToken
	int jour = Integer.parseInt(tabFrag[0]);
	int mois = Integer.parseInt(tabFrag[1]);
	int annee = Integer.parseInt(tabFrag[2]);
	return new Date(jour,mois,annee);
}

public boolean isToday() {
	return new Date().compareTo(this) == 0;
}

//A MODIFIER!!! En ann�es d'apr�s le sujet x) Ou au moins pouvoir convertir �a en ann�es
//Methode pour calculer la p�riode en jours entre deux dates 
public int differenceDates(Date parDate) { 
	int differenceDates = 0;
	Date date1 = this;
	Date date2 = parDate;
	while (date1.chDateCalendrier.before(date2.chDateCalendrier)) {
		date1.chDateCalendrier.add(date1.chDateCalendrier.DAY_OF_MONTH, 1);
		differenceDates++;
	}
	while (date1.chDateCalendrier.after(date2.chDateCalendrier)) {
		date1.chDateCalendrier.add(date2.chDateCalendrier.DAY_OF_MONTH, -1);
		differenceDates--;
	}
	return differenceDates;
	
} //difference Dates

}  // class Date