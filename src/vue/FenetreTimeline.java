package vue;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import modele.ExceptionDate;

public class FenetreTimeline extends JFrame {

	public FenetreTimeline(String parTitre) throws ExceptionDate {
		super(parTitre);
		this.setLayout(new BorderLayout());
		PanelTimeline contentPane = new PanelTimeline();
		setContentPane(contentPane);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
		setVisible(true);
		setLocation(100,100);
	}
	
	public static void main(String[]Args) {
		try {
			FenetreTimeline fenetre = new FenetreTimeline("fenetre");
		} catch (ExceptionDate e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
