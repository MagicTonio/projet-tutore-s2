package vue;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import modele.Timeline;

public class PanelFormulaire extends JPanel {

	JTextField fIntitule = new JTextField("Intitule");
	JTextField fDateDebut = new JTextField("dd mm aaaa");
	JTextField fDateFin = new JTextField("dd mm aaaa");
	JTextField fBoiteMessage = new JTextField(10);
	JTextField fNom = new JTextField("Nom");
	JTextField fDate = new JTextField("dd mm aaaa");
	JTextField fDesc = new JTextField("Desc");
	
	final String [] poids = {"1", "2", "3", "4"};
	JRadioButton[] boutonsPoids = new JRadioButton[poids.length];
	ButtonGroup groupeBoutonsPoids = new ButtonGroup();
	JButton boutonAjout = new JButton("+");
	
	public PanelFormulaire(Timeline parTimeline) {
		
		this.setLayout(new GridBagLayout());
		
		//Declaration et initialisation Labels
		JLabel labelTimeline = new JLabel("Timeline", JLabel.CENTER);
		
		JLabel labelIntitule = new JLabel("Intitule", JLabel.LEFT);
		labelIntitule.setDisplayedMnemonic('I');
		labelIntitule.setLabelFor(fIntitule);
		
		JLabel labelDateDebut = new JLabel("Date Debut", JLabel.LEFT);
		labelDateDebut.setDisplayedMnemonic('D');
		labelDateDebut.setLabelFor(fDateDebut);
		
		JLabel labelDateFin = new JLabel("Date Fin", JLabel.LEFT);
		labelDateFin.setDisplayedMnemonic('F');
		labelDateFin.setLabelFor(fDateFin);
		
		//JLabel labelBoiteMessages = new JLabel("");
		JLabel labelEvt = new JLabel("Evenement", JLabel.CENTER);
		
		JLabel labelDate = new JLabel("Date", JLabel.LEFT);
		labelDate.setDisplayedMnemonic('a');
		labelDate.setLabelFor(fDate);
		
		JLabel labelNom = new JLabel("Nom", JLabel.LEFT);
		labelNom.setDisplayedMnemonic('N');
		labelNom.setLabelFor(fNom);
		
		JLabel labelDesc = new JLabel("Description", JLabel.LEFT);
		labelDesc.setDisplayedMnemonic('e');
		labelDesc.setLabelFor(fDesc);
		
		JLabel labelPoids = new JLabel("Poids", JLabel.LEFT);
		
		// Ajout des �l�ments
		//AJOUT ET MISE EN FORME
		GridBagConstraints contrainte = new GridBagConstraints();
		contrainte.fill=GridBagConstraints.BOTH;
		contrainte.insets=new Insets(10,10,10,10);
		contrainte.ipadx=5; contrainte.ipady=2;
		
		//Labels
		contrainte.gridx=0; contrainte.gridy=0; contrainte.gridwidth=5;
		this.add(labelTimeline, contrainte);
		
		contrainte.gridx=0; contrainte.gridy=1; contrainte.gridwidth=1;
		this.add(labelIntitule, contrainte);
		contrainte.gridx=0; contrainte.gridy=2;
		this.add(labelDateDebut, contrainte);
		contrainte.gridx=0; contrainte.gridy=3;
		this.add(labelDateFin, contrainte);
		
		contrainte.gridx=0; contrainte.gridy=5; contrainte.gridwidth=5; contrainte.ipady=5;
		this.add(labelEvt, contrainte);
		contrainte.gridx=0; contrainte.gridy=6; contrainte.gridwidth=1; contrainte.ipady=2;
		this.add(labelNom, contrainte);
		contrainte.gridx=0; contrainte.gridy=7;
		this.add(labelDate, contrainte);
		contrainte.gridx=0; contrainte.gridy=8;
		this.add(labelDesc, contrainte);
		contrainte.gridx=0; contrainte.gridy=9;
		this.add(labelPoids, contrainte);
		
		contrainte.gridx=1; contrainte.gridy=1; contrainte.gridwidth=10;
		this.add(fIntitule, contrainte);
		contrainte.gridx=1; contrainte.gridy=2;
		this.add(fDateDebut, contrainte);
		contrainte.gridx=1; contrainte.gridy=3; 
		this.add(fDateFin, contrainte);
		contrainte.gridx=1; contrainte.gridy=4; contrainte.weighty=1;
		this.add(fBoiteMessage, contrainte);
		
		contrainte.gridx=1; contrainte.gridy=6; contrainte.weighty=0; contrainte.gridheight=1;
		this.add(fNom, contrainte);
		contrainte.gridx=1; contrainte.gridy=7; 
		this.add(fDate, contrainte);
		contrainte.gridx=1; contrainte.gridy=8; 
		this.add(fDesc, contrainte);
		
		//Ajout Boutons
		contrainte.gridx=0; contrainte.gridy=11; contrainte.weightx=1;
		this.add(boutonAjout, contrainte);
		
		for (int i=0; i < boutonsPoids.length; i++) {
			boutonsPoids[i] = new JRadioButton(poids[i]); //ajout des boutons
			boutonsPoids[i].setContentAreaFilled(false); //esthetique
			contrainte.gridy=9; contrainte.gridx=i+1; contrainte.gridwidth=1;contrainte.weightx=0;
			this.add(boutonsPoids[i], contrainte);
		} // for
		
		
	
		
	}//constructeur


}
