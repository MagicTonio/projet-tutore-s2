package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import modele.ExceptionTimeline;
import modele.LectureEcriture;
import modele.Timeline;
import vue.PanelFormulaire;

public class Controleur implements ActionListener {
	
	//Cr�er un tableau de timelines
	Timeline timeline;
	ArrayList <Timeline> listeTimelines;
	PanelFormulaire panelFormulaire;
	
	public Controleur (ArrayList <Timeline> parListeTime, PanelFormulaire parPanelFormulaire) {
		
		panelFormulaire = parPanelFormulaire;
		listeTimelines = parListeTime;
		
	}

	@Override
	public void actionPerformed(ActionEvent parEvt) {
		Timeline timelineForm;
		if (parEvt.getSource()=="+") {
			try {
				timeline = panelFormulaire.getTimeline();
				if(listeTimelines.contains(timeline)) { //Si la timeline en question existe d�j�
					timeline.ajout(panelFormulaire.getEvenement()); //On lui ajoute l'�v�nement
				}
				else { //si elle existe pas, on lui ajoute l'�v�nement et on l'ajoute � la liste des timelines
					timeline.ajout(panelFormulaire.getEvenement());
					listeTimelines.add(timeline);
				}
				System.out.println(LectureEcriture.lecture(timeline.chFichier));
			}
			 catch (ExceptionTimeline e) {
				e.printStackTrace();
			}
			
			
				
		} //if
		
	}
}
	

