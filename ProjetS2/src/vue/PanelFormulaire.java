package vue;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import controleur.Controleur;
import modele.Date;
import modele.Evenement;
import modele.ExceptionTimeline;
import modele.Timeline;

public class PanelFormulaire extends JPanel {

	JTextField fIntitule = new JTextField("Intitule");
	JTextField fDateDebut = new JTextField("dd mm aaaa");
	JTextField fDateFin = new JTextField("dd mm aaaa");
	JTextField fBoiteMessage = new JTextField(10);
	JTextField fNom = new JTextField("Nom");
	JTextField fDate = new JTextField("dd mm aaaa");
	JTextField fDesc = new JTextField("Desc");
	
	final String [] poids = {"1", "2", "3", "4"};
	JRadioButton[] boutonsPoids = new JRadioButton[poids.length];
	ButtonGroup groupeBoutonsPoids = new ButtonGroup();
	JButton boutonAjout = new JButton("+");
	
	public PanelFormulaire(Timeline parTimeline) {
		
		this.setLayout(new GridBagLayout());
		
		//Declaration et initialisation Labels
		JLabel labelTimeline = new JLabel("Timeline", JLabel.CENTER);
		
		JLabel labelIntitule = new JLabel("Intitule", JLabel.LEFT);
		labelIntitule.setDisplayedMnemonic('I');
		labelIntitule.setLabelFor(fIntitule);
		
		JLabel labelDateDebut = new JLabel("Date Debut", JLabel.LEFT);
		labelDateDebut.setDisplayedMnemonic('D');
		labelDateDebut.setLabelFor(fDateDebut);
		
		JLabel labelDateFin = new JLabel("Date Fin", JLabel.LEFT);
		labelDateFin.setDisplayedMnemonic('F');
		labelDateFin.setLabelFor(fDateFin);
		
		//JLabel labelBoiteMessages = new JLabel("");
		JLabel labelEvt = new JLabel("Evenement", JLabel.CENTER);
		
		JLabel labelDate = new JLabel("Date", JLabel.LEFT);
		labelDate.setDisplayedMnemonic('a');
		labelDate.setLabelFor(fDate);
		
		JLabel labelNom = new JLabel("Nom", JLabel.LEFT);
		labelNom.setDisplayedMnemonic('N');
		labelNom.setLabelFor(fNom);
		
		JLabel labelDesc = new JLabel("Description", JLabel.LEFT);
		labelDesc.setDisplayedMnemonic('e');
		labelDesc.setLabelFor(fDesc);
		
		JLabel labelPoids = new JLabel("Poids", JLabel.LEFT);
		
		// Ajout des �l�ments
		//AJOUT ET MISE EN FORME
		GridBagConstraints contrainte = new GridBagConstraints();
		contrainte.fill=GridBagConstraints.BOTH;
		contrainte.insets=new Insets(10,10,10,10);
		contrainte.ipadx=5; contrainte.ipady=2;
		
		//Labels
		contrainte.gridx=0; contrainte.gridy=0; contrainte.gridwidth=5;
		this.add(labelTimeline, contrainte);
		
		contrainte.gridx=0; contrainte.gridy=1; contrainte.gridwidth=1;
		this.add(labelIntitule, contrainte);
		contrainte.gridx=0; contrainte.gridy=2;
		this.add(labelDateDebut, contrainte);
		contrainte.gridx=0; contrainte.gridy=3;
		this.add(labelDateFin, contrainte);
		
		contrainte.gridx=0; contrainte.gridy=5; contrainte.gridwidth=5; contrainte.ipady=5;
		this.add(labelEvt, contrainte);
		contrainte.gridx=0; contrainte.gridy=6; contrainte.gridwidth=1; contrainte.ipady=2;
		this.add(labelNom, contrainte);
		contrainte.gridx=0; contrainte.gridy=7;
		this.add(labelDate, contrainte);
		contrainte.gridx=0; contrainte.gridy=8;
		this.add(labelDesc, contrainte);
		contrainte.gridx=0; contrainte.gridy=9;
		this.add(labelPoids, contrainte);
		
		contrainte.gridx=1; contrainte.gridy=1; contrainte.gridwidth=10;
		this.add(fIntitule, contrainte);
		contrainte.gridx=1; contrainte.gridy=2;
		this.add(fDateDebut, contrainte);
		contrainte.gridx=1; contrainte.gridy=3; 
		this.add(fDateFin, contrainte);
		contrainte.gridx=1; contrainte.gridy=4; contrainte.weighty=1;
		this.add(fBoiteMessage, contrainte);
		
		contrainte.gridx=1; contrainte.gridy=6; contrainte.weighty=0; contrainte.gridheight=1;
		this.add(fNom, contrainte);
		contrainte.gridx=1; contrainte.gridy=7; 
		this.add(fDate, contrainte);
		contrainte.gridx=1; contrainte.gridy=8; 
		this.add(fDesc, contrainte);
		
		//Ajout Boutons
		contrainte.gridx=0; contrainte.gridy=11; contrainte.weightx=1;
		this.add(boutonAjout, contrainte);
		
		for (int i=0; i < boutonsPoids.length; i++) {
			boutonsPoids[i] = new JRadioButton(poids[i]); //ajout des boutons
			boutonsPoids[i].setContentAreaFilled(false); //esthetique
			contrainte.gridy=9; contrainte.gridx=i+1; contrainte.gridwidth=1;contrainte.weightx=0;
			this.add(boutonsPoids[i], contrainte);
			groupeBoutonsPoids.add(boutonsPoids[i]);
		} // for
		boutonsPoids[0].setSelected(true); //Le premier bouton (1) sera s�lectionn� par d�faut
		
		
		
	}//constructeur
	
	//public Evenement getEvenement() {
	//	return new Evenement (getDate(), fNom.getText(), )
	//}
	
	public void enregristreEcouteur(Controleur parControleur) {
		boutonAjout.addActionListener(parControleur);
		fIntitule.addActionListener(parControleur);
		fDateDebut.addActionListener(parControleur);
		fDateFin.addActionListener(parControleur);
		fNom.addActionListener(parControleur);
		fDate.addActionListener(parControleur);
		fDesc.addActionListener(parControleur);
		
		for (JRadioButton boutonP : boutonsPoids) { //Ca marche pas de le faire pour le groupe individuellement lol 
			boutonP.addActionListener(parControleur);
		}
	} //enregistreEcouteur
	
	//ACCESSEURS
	public Date getDate(JTextField parF) {
		JTextField chField = parF;
		String[] chaine = chField.getText().split(" "); //Prend le texte du TextField et s�pare la chaine de caract�re en mini-chaines � chaque espace
		//Ensuite range chaque petit fragment dans un tableau de chaines. Pour chaine[0], il y aura le jour en string, chaine[1] le mois, etc ;)
		int jour = Integer.parseInt(chaine[0]); //On convertit le jour en string en int
		int mois = Integer.parseInt(chaine[1]);
		int annee = Integer.parseInt(chaine[2]);
		
		return new Date(jour, mois, annee); //Ca devrait raise une erreur si le format de la date est pas bion, cf classe Date
	}
	
	public int getPoids() {
		
		if (groupeBoutonsPoids.getSelection().getActionCommand() == "2")
			return 2;
		
		if (groupeBoutonsPoids.getSelection().getActionCommand() == "3")
			return 3; 
		
		if (groupeBoutonsPoids.getSelection().getActionCommand() == "4")
			return 4; 
		return 1; //Par d�faut, on retourne 1 (1 est toujours appuy� par d�faut
	}
	
	public Timeline getTimeline() throws ExceptionTimeline { //Raise exception sur la timeline si les dates sont pas bonnes
		return new Timeline(this.getDate(fDateDebut), this.getDate(fDateFin), fIntitule.getText());
	}
	
	public Evenement getEvenement() {
		return new Evenement(getDate(fDate), fNom.getText(), getPoids(), fDesc.getText());
	}
	
	//MODIFIEURS
	
}
