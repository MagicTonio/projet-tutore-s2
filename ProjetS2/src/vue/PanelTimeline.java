package vue;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import controleur.Controleur;
import modele.Date;
import modele.Evenement;
import modele.ExceptionTimeline;
import modele.LectureEcriture;
import modele.Timeline;

public class PanelTimeline extends JPanel{
	public PanelTimeline() {
		this.setSize(400,700);
		ArrayList <Timeline> listeTimelines= new ArrayList();
		PanelFormulaire panelFormulaire;
		PanelAffichage panelAffichage;
		Controleur controleur;
		
		try {
			Timeline time = new Timeline(new Date(22, 5, 2018), new Date(28, 5, 2018), "timeline1");
			time.ajout(new Evenement(new Date(23,5,2018), "Evt1", 1, "Nowhere"));
			time.ajout(new Evenement(new Date(23,5,2018), "idk", 2, "Nowhere"));
			time.ajout(new Evenement(new Date(24,5,2018), "whatidk", 1, "Nowhere"));
			time.ajout(new Evenement(new Date(22,5,2018), "whatidk", 2, "Nowhere"));
			LectureEcriture.ecriture(time.chFichier, time);
			listeTimelines.add(time);
			System.out.println(LectureEcriture.lecture(time.chFichier));

			this.setLayout(new BorderLayout());
			
			panelFormulaire = new PanelFormulaire(time);
			this.add(panelFormulaire, BorderLayout.WEST);
			
			panelAffichage = new PanelAffichage(time);
			this.add(panelAffichage, BorderLayout.CENTER);
			
			controleur=new Controleur(listeTimelines, panelFormulaire);
			
		} catch (ExceptionTimeline e) {
			e.getMessage();
		}
		
		
		//System.out.println(time.toString());
		
		
	}
}
