package vue;

import javax.swing.ImageIcon;
//ADDED VER 2.0
import javax.swing.JLabel;
import javax.swing.JPanel;

import modele.Evenement;

public class PanelEvenement extends JPanel{
	Evenement chEvenement;
	JLabel labelNomEvt;
	JLabel labelDescEvt;
	JLabel labelImage;
	ImageIcon image;
	
	public PanelEvenement(Evenement parEvt) {
		labelNomEvt = new JLabel(parEvt.getNom()); 
		labelDescEvt = new JLabel(parEvt.getDesc()); 
		image = new ImageIcon(parEvt.getImagePath());
		JLabel labelImage = new JLabel(image);
		
		this.add(labelNomEvt);
		this.add(labelDescEvt);
		this.add(labelImage);
	}
}
