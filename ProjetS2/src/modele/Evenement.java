package modele;

import java.io.File;
import java.io.Serializable;

//AJOUTER LE CHEMIN DE L'IMAGE EN PARAMETRES mais flemme de le faire
//Faut modifier le formulaire pour ajouter les champs, Evenement, Timeline et partout o� elles sont utilis�es...

public class Evenement implements Comparable <Evenement>, Serializable {
	
	private Date chDate;
	private String chNom;
	private String chDesc;
	private int chPoids;
	private String chImagePath;
	
	public Evenement(Date parDate, String parNom, int parPoids, String parDesc) {
		chDate = parDate;
		chNom = parNom;
		chDesc = parDesc; //Champ description, le Lieu n'est pas important d'apr�s l'�nonc� du projet
		chPoids = parPoids; //Importance de l'�v�nement, de 1 (le plus important) � 4 (moins important)
		chImagePath = "images"+ File.separator + chNom + ".jpg";
	} //constructeur
	
	public String toString() {
		return chDate.toString() + ", " + chNom + ", " + chDesc; //Affiche l'�v�nement, un classique
	}
	
	///////////////////////Accesseurs///////////////////////////////////////////
	
	public Date getDate() {
		return chDate;
	}
	
	public String getNom() {
		return chNom;
	}
	
	public String getDesc() {
		return chDesc;
	}
	
	public String getImagePath() {
		return chImagePath;
	}
	
	/////////////////////////////Modifieurs////////////////////////////////////////
	
	public void setDate(int parJour, int parMois, int parAnnee) throws ExceptionDate {
		chDate = new Date(parJour, parMois, parAnnee);
	}
	
	public void setNom(String parNom) {
		chNom = parNom;
	}
	
	public void setDesc(String parDesc) {
		chDesc = parDesc;
	}
	////////////////////////////////////////////////////////////////////////////////
	//Alors �a nous sert � rien je crois donc on peut le barrer mais je le farde au cas o�, du coup on pourra enlever la classe Clavier aussi
	
	/*public static Evenement lireEvenement() throws ExceptionDate {
		Date date = Date.lireDate();
		System.out.println("Entrez le nom de l'�v�nement");
		String nom = Clavier.lireString();
		System.out.println("Entrez la Description");
		String desc = Clavier.lireString();
		System.out.println("Entrez l'importance de l'�v�nement, de 1 moins important � x plus important"); //a modif
		int poids = Clavier.lireInt();
		
		return new Evenement(date, nom, poids, desc);
	} // lire Evenement */
	
	
	//CompareTo modifi� pour d'abord prendre en compte l'importance de l'�v�nement
	// Est-ce que �a doit prendre en compte la description? Je l'ai pas mise parce que si les �v�nements ont la m�me date et le m�me nom �a sert � rien d'avoir des doublons
	public int compareTo(Evenement parEvt) {
		if (chDate.compareTo(parEvt.getDate()) == -1)
			return -1;
		if (chDate.compareTo(parEvt.getDate()) == 1)
			return 1;
		// Les dates sont �gales, on regarde le poids pour savoir comment les classer
		if (chDate.compareTo(parEvt.getDate()) == 0) {
			if(this.chPoids<parEvt.chPoids)
				return -1;
			if(this.chPoids>parEvt.chPoids)
				return 1;
			//Ensuite on regarde le nom au cas o� y'a deux �v�nements de m�me poids, mais normalement �a devrait pas arriver
			if (chNom.compareTo(parEvt.getNom()) == -1)
				return -1;
			if (chNom.compareTo(parEvt.getNom()) == 1)
				return 1;
			if (chNom.compareTo(parEvt.getNom()) == 0) {
					return 0;
			}
		}
		return -1;
	} //compareTo
}
