package modele;

import java.io.File;
import java.io.Serializable;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**VER2.0
* AJOUT ACCESSEURS TIMELINE, pour une meilleure encapsulation. les champs sont tous devenus private
* AJOUT PANEL AFFICHAGE (Cardlayout)
* Cr�ation du contr�leur
* AJOUT DES IMAGES dans la classe �v�nement 
* 
/** Classe principale, qui d�finit la frise.
 * J'ai omis le fichier de sauvegarde dans les param�tres, parce que c'est plus simple de sauvegarder par d�faut dans un fichier dont le nom est le nom de la frise.
 * Pareil pour la p�riode, j'ai fait une m�thode qui la calcule directement dans la classe Date. Par contre j'ai fait �a en nombre de jours sauf que c'est un peu le bordel si la p�riode fait quelques ann�es... Je changerai
 
 */

public class Timeline implements Serializable {
	
	private TreeMap <Date, TreeSet <Evenement>> mapDates; //Tree Set car il peut y avoir plusieurs Evenements pour une m�me date, tri�s en fonction du poids
	private String chNom;
	private Date chDateDebut;
	private Date chDateFin;
	private int nbEvenements; //public pour y avoir acces dans les autres classes aussi
	public File chFichier; //champ pour la sauvegarde du fichier
	
	public Timeline (Date parDateDebut, Date parDateFin, String parNom) throws ExceptionTimeline { //Raise error si DateDebut sup�rieure � dateFin, logique
		
		if(parDateDebut.compareTo(parDateFin) > 0)
			throw new ExceptionTimeline("La date de d�but doit �tre inf�rieure � la date de fin du timeline");
		
		chNom = parNom;
		chDateDebut = parDateDebut;
		chDateFin = parDateFin;
		mapDates = new TreeMap <Date, TreeSet<Evenement>> ();
		nbEvenements = 0;
		chFichier=new File("timelines" + File.separator + parNom+".ser");
		
	} //Constructeur
	
	
	public void ajout(Evenement parEvt) throws ExceptionTimeline {
		if (parEvt.getDate().compareTo(chDateDebut)<0 || parEvt.getDate().compareTo(chDateFin) >0) //Si la date de l'�v�nement est en dehors de la frise
			throw new ExceptionTimeline("Entrez une date comprise entre les dates de d�but et fin du timeline");
		
		TreeSet <Evenement> evtParDate; //TreeSet qui classe tous les �v�nements pour une m�me date (automatiquement en fonction du poids, cf CompareTo
		Date dateEvt = parEvt.getDate();
		
		if (mapDates.containsKey(dateEvt)) { // Si y'a d�j� des �v�nements � la date de l'�v�nement qu'on veut ajouter, �a veut dire que le treSet existe d�j�
			evtParDate = mapDates.get(dateEvt); //Donc il va chercher le treeSet correspondant et il le met dedans
			evtParDate.add(parEvt);
		}
		
		else { //si le treeSet n'existe pas, on le cr�e
			evtParDate = new TreeSet <Evenement>();
			evtParDate.add(parEvt); //et on ajoute l'�v�nement dans le TreeSet vide
		}
		mapDates.put(dateEvt, evtParDate); //on ajoute le couple Date, TreeSet d'evts pour cette date dans le TreeMap
		nbEvenements = nbEvenements+1; //et on augmente le nombre d'�v�nements
	}
	
	
	public String toString() {
		String chaine = "";
		Set <Date> cles = mapDates.keySet(); //Les dates sont les cl�s du TreeMap, pour une m�me date y'a plusieurs �venements class�s par poids dans un TreeSet
		TreeSet <Evenement> listeTreeMap = new TreeSet<Evenement>();
		Iterator <Evenement> iterator; //On d�clare l'it�rateur mais ne l'initialise pas

		for (Date cle : cles) { //Ca parcourt toutes les dates une par une
			chaine = chaine + "\n Date " + cle.toString() + " :"; //"Date mardi 22 mai :" par ex 
			listeTreeMap = mapDates.get(cle); //va chercher le TreeSet d'�v�nements correspondant � chaque date
			iterator = listeTreeMap.iterator(); //L'it�rateur va parcourir chaque treeSet du coup
			while (iterator.hasNext()) {
				chaine = chaine + " " + "["+ iterator.next().toString() + "]"; //On affiche � chaque fois chaque �v�nement du TreeSet en question
			} //while
		}
		return chaine;
	} //toString
	
	public int calculPeriode(){
		int periode = chDateDebut.differenceDates(chDateFin);
		return periode;
	}
	//Accesseurs
	public int getNbEvenements() {
		return nbEvenements;
	}
	
	public String getNom() {
		return chNom;
	}
	
	public TreeMap<Date, TreeSet<Evenement>> getTreeMap(){ //Utile ou pas? A voir... Utilis�e dans PanelAffichage
		return mapDates;
	}
	
}
