package test;

import java.io.File;

import modele.Date;
import modele.Evenement;
import modele.ExceptionTimeline;
import modele.LectureEcriture;
import modele.Timeline;

public class TestTimeline {
	public static void main(String[]args) {
		try {
			Timeline time = new Timeline(new Date(22, 5, 2018), new Date(28, 5, 2018), "timeline1");
			time.ajout(new Evenement(new Date(23,5,2018), "Evt1", 1, "Nowhere"));
			time.ajout(new Evenement(new Date(23,5,2018), "idk", 2, "Nowhere"));
			time.ajout(new Evenement(new Date(24,5,2018), "whatidk", 1,"Nowhere"));
			time.ajout(new Evenement(new Date(22,5,2018), "whatidk", 2, "Nowhere"));
			
			//System.out.println(time.toString());
			
			LectureEcriture.ecriture(time.chFichier, time);
			System.out.println(LectureEcriture.lecture(time.chFichier));
			
			
		} catch (ExceptionTimeline e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
